<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__).'/functions.php';
require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';

function submit_recipientsField($label, $name)
{
    $W = bab_Widgets();
    
    return $W->LabelledWidget($label, $W->LineEdit()->addClass('widget-100pc'), $name);
}



function submit_getOptionsForm()
{
    $W = bab_Widgets();
    $form = $W->Form(null, $W->VBoxLayout()->setVerticalSpacing(2, 'em'));
    $form->setHiddenValue('tg', bab_rp('tg'));
    $form->setName('options');
    
    $form->addItem(submit_recipientsField(submit_translate('To'), 'to'));
    $form->addItem(submit_recipientsField(submit_translate('Cc'), 'cc'));
    $form->addItem(submit_recipientsField(submit_translate('Bcc'), 'bcc'));
    
    $form->addItem($W->LabelledWidget(
        submit_translate('Require a valid captcha code on all anonymous requests'), 
        $W->Select()->setOptions(array('' => '', 'addon' => 'Ovidentia addon', 'recaptcha' => 'reCaptcha')),
        'captcha'
    ));
    
    $form->addItem($W->LabelledWidget(submit_translate('ReCaptcha secret'), $W->LineEdit()->addClass('widget-100pc'), 'reCapchaSecret'));
    
    $form->addItem(
        $W->SubmitButton()
        ->setLabel(submit_translate('Save'))
    );
    
    $form->addClass('widget-bordered');
    
    return $form;
}



function submit_displayOptionsPage()
{
    $W = bab_Widgets();
    $page = $W->BabPage();
    
    $form = submit_getOptionsForm();
    $registry = submit_getRegistry();
    $form->setValues(array(
        'options' => array(
            'to' => $registry->getValue('to', ''),
            'cc' => $registry->getValue('cc', ''),
            'bcc' => $registry->getValue('bcc', ''),
            'captcha' => $registry->getValue('captcha', false),
            'reCapchaSecret' => $registry->getValue('reCapchaSecret', '')
        )
    ));
    $page->addItem($form);
    $page->displayHtml();
}



function submit_saveOptions()
{
    $values = bab_pp('options');
    
    $registry = submit_getRegistry();
    $registry->setKeyValue('to', $values['to']);
    $registry->setKeyValue('cc', $values['cc']);
    $registry->setKeyValue('bcc', $values['bcc']);
    $registry->setKeyValue('captcha', $values['captcha']);
    $registry->setKeyValue('reCapchaSecret', $values['reCapchaSecret']);
    
    $next = bab_url::get_request('tg');
    $next->location();
}




if (!bab_isUserAdministrator()) {
    exit;
}

if (!empty($_POST)) {
    submit_saveOptions();
}

submit_displayOptionsPage();