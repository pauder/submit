; <?php/*

[general]
name				="submit"
version				="0.0.9"
addon_type			="EXTENSION"
addon_access_control=0
encoding			="UTF-8"
mysql_character_set_database="latin1,utf8"
description			="Submit form to email"
description.fr		="Envoi d'un formulaire vers un email"
delete				=1
ov_version			="8.2.0"
php_version			="5.3.0"
author				="Cantico"
icon				="contact.png"
configuration_page	="admin"
tags                ="extension,mail"

[addons]
LibTranslate        = ">=1.12.0rc3.01"
widgets             = ">=1.0.110"

;*/ ?>