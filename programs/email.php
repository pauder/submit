<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__).'/functions.php';
require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';

class submit_DisplayException extends Exception {
    
}

class submit_MailException extends submit_DisplayException {
}


function submit_getSectionTitle(Array $data, $level) {
    if (!isset($data['name'])) {
        return '';
    }
    
    return sprintf('<h%d class="submit-title">%s</h%d>', $level, bab_toHtml($data['name']), $level);
}

/**
 * @return string
 */
function submit_getField($arr) {
    if (!is_array($arr)) {
        return '';
    }
    
    if (!isset($arr['name'])|| !isset($arr['value'])) {
        return '';
    }
    
    return sprintf('<p class="submit-field"><strong>%s</strong><br />%s</p>', 
        bab_toHtml($arr['name']), bab_toHtml($arr['value']));
}



/**
 * Return true if this is an array with no value key
 * @return boolean
 */
function submit_isSection($arr) {
    if (!is_array($arr)) {
        return false;
    }
    
    return !isset($arr['value']);
}



/**
 * Get recursive html for an array
 * @param array $data
 * @return string
 */
function submit_getHtml(Array $data, $level) {
    
    $html = '';
    
    if ($level > 6) {
        $level = 6;
    }

    
    foreach ($data as $value) {
        if (submit_isSection($value)) {
            $html .= submit_getSectionTitle($value, $level);
            $html .= submit_getHtml($value, ++$level);
            continue;
        }
        
        // add field
        $html .= submit_getField($value);
    }
    
    return $html;
}




function submit_getPostedSender(Array $post)
{
    foreach ($post as $name => $value) {
        if ('email' === $name || 'mail' === $name || 'sender' === $name) {
            return $value['value'];
        }
        
        if (is_array($value) && !isset($value['value'])) {
            if ($email = submit_getPostedSender($value)) {
                return $email;
            }
        }
    }
    
    return null;
}



function submit_AddRecipients(babMail $mail, $methodName, $strRecipients) {
    
    if (empty($strRecipients)) {
        return;
    }
    
    $list = preg_split('/\s*[,;]\s*/', $strRecipients);
    foreach ($list as $email) {
        $email = trim($email);
        $mail->$methodName($email);
    }
}


/**
 * Verify recapcha code
 * @param string $code ReCapcha security token
 * @return bool
 */
function submit_isReCaptchaValid($code) {
    $registry = submit_getRegistry();

    $opts = array('http' =>
        array(
            'method'  => 'POST',
            'header'  => 'Content-type: application/x-www-form-urlencoded',
            'content' => http_build_query(array(
                'secret'   => $registry->getValue('reCapchaSecret'),
                'response' => $code
            ))
        )
    );
    $context  = stream_context_create($opts);
    $result = file_get_contents('https://www.google.com/recaptcha/api/siteverify', false, $context);
    if (false === $result) {
        return false;
    }
    $check = json_decode($result);
    
    return $check->success;
}


function submit_CheckCaptcha() {
    if (bab_isUserLogged()) {
        return;
    }
    
    $registry = submit_getRegistry();
    $captchaType = $registry->getValue('captcha');
    if (!$captchaType) {
        return;
    }
    
    if ('addon' === $captchaType) {
        if (!bab_functionality::get('Captcha')->securityCodeValid(bab_pp('captchaSecurityCode'))) {
            throw new submit_DisplayException(submit_translate('The captcha value is incorrect'));
        }
        return;
    }
    
    if ('recaptcha' === $captchaType) {
        if (!submit_isReCaptchaValid(bab_pp('captchaSecurityCode'))) {
            throw new submit_DisplayException(submit_translate('The captcha value is incorrect'));
        }
        return;
    }
    
    throw new submit_DisplayException('Unexpected captcha type '.$captchaType);
}



function submit_recipient($name) {
    $registry = submit_getRegistry();
    return $registry->getValue($name);
}



/**
 * 
 * subject = 'Subject'
 * data[name] = 'Main Title'
 * data[lastname][name] = 'Lastname'
 * data[lastname][value] = ''
 * data[firstname][name] = 'Firstname'
 * data[firstname][value] = ''
 * data[address][name] = 'Adress Section title'
 * data[address][street][name] = 'Street'
 * data[address][street][value] = ''
 * data[address][city][name] = 'City'
 * data[address][city][value] = ''
 */
function submit_Send() {
    bab_requireSaveMethod();
    
    require_once $GLOBALS['babInstallPath'].'utilit/mailincl.php';
    require_once $GLOBALS['babInstallPath'].'utilit/settings.class.php';
    
    $settings = bab_getInstance('bab_Settings');
    /*@var $settings bab_Settings */
    $site = $settings->getSiteSettings();
    
    $mail = bab_mail();
    
    if (!$mail) {
        throw new submit_DisplayException(submit_translate('Mail is not configured'));
    }
    
    if ($to = submit_recipient('to')) {
        submit_AddRecipients($mail, 'mailTo', $to);
    } else {
        $mail->mailTo($site['adminemail'], $site['adminname']);
    }
    
    submit_AddRecipients($mail, 'mailCc', submit_recipient('cc'));
    submit_AddRecipients($mail, 'mailBcc', submit_recipient('bcc'));
    
    $mail->mailSender($site['adminemail']);
    $mail->mailSubject(bab_pp('subject', sprintf(submit_translate('Message from %s'), $_SERVER['HTTP_HOST'])));
    $mail->mailBody($mail->mailTemplate(submit_getHtml($_POST, 2)), 'html');
    $mail->send();
}


$babBody = bab_getBody();

try {
    submit_CheckCaptcha();
    submit_Send();
} catch(submit_DisplayException $e) {
    $babBody->addError($e->getMessage());
    return;
}


$babBody->addNextPageMessage(bab_pp('successMessage', submit_translate('Your message has been sent')));

// redirect to success page

$next = new bab_url(bab_pp('nextPage', bab_getAddonInfosInstance('submit')->getUrl().'success'));
$next->location();
