��          |      �             !     2     6     9     P     `  6   q     �     �     �     �    �     �            &   %     L     Z  V   k     �     �     �     �                                    
             	          Back to homepage Bcc Cc Mail is not configured Message from %s ReCaptcha secret Require a valid captcha code on all anonymous requests Save The captcha value is incorrect To Your message has been sent Project-Id-Version: submit
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-09-08 08:02+0200
Last-Translator: Paul de Rosanbo <paul.derosanbo@cantico.fr>
Language-Team: Cantico <paul.derosanbo@cantico.fr>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-SourceCharset: iso-8859-1
X-Poedit-Basepath: ..
X-Poedit-KeywordsList: submit_translate;submit_translate:1,2
X-Generator: Poedit 2.3
X-Poedit-SearchPath-0: programs
 Retour à l'accueil Copie cachée à Copie à L'envoi des mails n'est pas configuré Message de %s Secret ReCaptcha Requiert un captcha valide sur les requêtes effectuées par des utilisateurs anonymes Enregistrer CAPTCHA invalide Au Votre message a été envoyé 